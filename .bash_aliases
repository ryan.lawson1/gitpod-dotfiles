alias redis-start='docker run --rm -d --name redis -p 6379:6379 redis'
alias redis-stop='docker stop redis'
alias psql-local='psql postgresql://postgres:docker@localhost:5432'
alias postgres-start='docker run --rm --name postgres -v /workspace/remote-dev/postgres/data:/var/lib/postgresql/data -d -e POSTGRES_PASSWORD=localhost123 -p 5432:5432 postgres:13.2-alpine'
alias postgres-stop='docker stop postgres'


alias pf='function pf () {aws --profile staging-eks eks update-kubeconfig --name snaptravel-staging-eks; kubectl port-forward service/$1 8080:80;};pf'

git config --global alias.test '!PIPENV_DOTENV_LOCATION=.env.test pipenv run git-test'

function dev_gitlint_setup(){
	echo "Setting up git lint command"
	pip install pylint
	pip3 install git+https://github.com/snaptravel/git-lint.git
}

function setup_payments_files(){
	cp -a ~/.config/coderv2/dotfiles/repo-files/payments/.* ./
}
function setup_payments(){
	echo "Setting up Payments repo"
	setup_payments_files
	super local 3.9.9
	docker-compose pull
}
function start_payments(){
	echo "Starting payments services. Use the VSCode debugger to run the service"
	docker-compose up -d postgres redis adminer
}

function setup_static_pages_node_env(){
	nvm install 10.23.0
	nvm use 10.23.0
}
function setup_static_pages_dotenv(){
	super vault get static-pages staging > .env
}
function setup_static_pages(){
	echo "Setting up static-pages repo"
	setup_static_pages_node_env
	setup_static_pages_dotenv
	npm install -g yarn
	yarn
}

function setup_front_end_node_env(){
	nvm install v18.15.0
	nvm use v18.15.0
}
function setup_front_end_dotenv(){
	super vault get front-end staging --dev-overwrite -o .env
}
function setup_front_end(){
	echo "Setting up front_end repo"
	setup_front_end_node_env
	setup_front_end_dotenv
	npm install -g yarn
	yarn
}

function setup_monorepo_e2e_dotenv(){
	yarn env-refresh
}

function setup_monorepo_e2e(){
	echo "Setting up monorepo/e2e"
	setup_front_end_node_env
	npm install -g yarn
	yarn
	setup_monorepo_e2e_dotenv
}

export PIPENV_VENV_IN_PROJECT=1

export STAGING_POSTGRES_HOST="vpc-remote-hotel-staging.c0qfsszbrxht.us-east-1.rds.amazonaws.com"

alias rds-get-credentials-staging="aws --profile rds-admin rds generate-db-auth-token --hostname $STAGING_POSTGRES_HOST --port 5432 --region us-east-1 --username administrator"

# https://snaptravelinc.atlassian.net/wiki/spaces/RD/pages/1147273223/Administering+Kubernetes+via+Kubectl+Helm+and+Lens
alias switch-to-staging="aws --profile staging-eks eks update-kubeconfig --name snaptravel-staging-eks"
alias switch-to-production-ro="aws --profile production-eks-readonly eks update-kubeconfig --name snaptravel-production-eks"
alias switch-to-production="aws --profile production-eks eks update-kubeconfig --name snaptravel-production-eks"

function setup_kubeconfig_stating() {
	kubectl get deployments -n $1 > /dev/null
	if [ $? -eq 0 ]
	then
		echo "Kubeconfig already set up"
		return 0
	fi
	aws-eks-staging
}

function pfk8 () {
	NAMESPACE="default"
	setup_kubeconfig_stating $NAMESPACE
	if [ -z "$1" ]
	then
		echo "No deployment specified. Please select one:"
		kubectl get deployments -n $NAMESPACE
		return 1
	fi
	LOCAL_PORT=$2
	if [ -z "$2" ]
	then
		echo "Using local port 8080"
		LOCAL_PORT=8080
	fi
	SERVICE_PORT=$3
	if [ -z "$3" ]
	then
		echo "Using service port 5000"
		SERVICE_PORT=5000
	fi
	kubectl -n $NAMESPACE port-forward deployment/$1 $LOCAL_PORT:$SERVICE_PORT;
}

export GITLAB_TEXTEMMA_URL='https://gitlab.com/textemma'
export GITLAB_TEXTEMMA_URL_SSH='git@gitlab.com:textemma'

alias poetry-venv-local='poetry config virtualenvs.in-project true'

function get_current_commit(){
	git rev-parse HEAD
}

alias gccommit='get_current_commit'

export CURRENT_COMMIT_HASH=$(get_current_commit)

function bfmt(){
	git status --porcelain | grep -E '(M|A)' | grep -E '\.py$' | cut -c4- | xargs -I {} sh -c 'echo "Formatting {}"; PYENV_VERSION=3.9.9 black {};'
}


function clonerepos(){
	# each arg is a repo name
	for repo in "$@"
	do
		super clone $repo /workspace/$repo
	done
}

function clonerepos-all-t14(){
	clonerepos providers_hub rates-v2 partner_search pricing pricing-optimizer supplier_rule
}

# Redis instances

alias redis-hotels3-ro-cli='docker run --rm -it redis redis-cli -h hotels3-ro.u12xq6.ng.0001.use1.cache.amazonaws.com'


function code-clone(){
	curl -Lk 'https://code.visualstudio.com/sha/download?build=stable&os=cli-alpine-x64' --output vscode_cli.tar.gz
	tar -xf vscode_cli.tar.gz
	chmod +x ./code
}

function code-tunnel(){
	# If code command doesn't exist, run code-clone
	code --version > /dev/null
	if [ $? -ne 0 ]
	then
		code-clone
	fi
	code tunnel
}