#!/bin/bash
. .bash_aliases

shopt -s dotglob

for f in ~/.config/coderv2/dotfiles/.*
do
	echo "Linking $f"
	ln -s $f ~/
done

cp ~/.config/coderv2/dotfiles/.gitconfig ~/.gitconfig

shopt -u dotglob