#!/bin/bash

# coder list
# WORKSPACE                             TEMPLATE     STATUS   HEALTHY  LAST BUILT  CURRENT VERSION  OUTDATED  STARTS AT  STOPS AFTER  
#   ryanlawson/2024Q3S4                 kubernetes   Stopped           16d         a821b774-master  false                9h           
#   ryanlawson/2024Q4                   kubernetes   Stopped           16d         a821b774-master  false                9h           
#   ryanlawson/amethyst-grasshopper-67  kubernetes   Stopped           14d21h      a821b774-master  false                9h           
#   ryanlawson/aquamarine-scorpion-37   kubernetes   Stopped           4d14h       a821b774-master  false                9h           
#   ryanlawson/crimson-anglerfish-7     kubernetes   Stopped           19d         a821b774-master  false                9h           
#   ryanlawson/crimson-meadowlark-54    kubernetes   Stopped           11d20h      a821b774-master  false                9h           
#   ryanlawson/cyan-gayal-39            kubernetes   Stopped           14d22h      a821b774-master  false                9h           
#   ryanlawson/emerald-hookworm-33      kubernetes   Stopped           <1m         a821b774-master  false                9h           
#   ryanlawson/fuchsia-shrew-59         kubernetes   Stopped           13d23h      a821b774-master  false                9h           
#   ryanlawson/lime-pelican-7           kubernetes   Stopped           14d15h      a821b774-master  false                9h           
#   ryanlawson/olive-asp-34             kubernetes   Stopped           12d19h      a821b774-master  false                9h           
#   ryanlawson/purple-giraffe-24        kubernetes   Stopped           12d15h      a821b774-master  false                9h           
#   ryanlawson/sapphire-perch-84        secret-sync  Stopped           14d6h       6fcadb80-master  false                             
#   ryanlawson/scarlet-otter-17         kubernetes   Stopped           12d5h       a821b774-master  false                9h

# Run the coder list command and get all the workspace names which are greader than 14 days old

# Get the list of workspaces
workspaces=$(coder list | grep 'kubernetes' | awk '{print $1}' | grep -v WORKSPACE)
declare -i CLEANUP_DAYS=7

echo "Cleanup workspaces older than $CLEANUP_DAYS days"

to_delete=()

# Loop through the workspaces
for workspace in $workspaces
do
  echo "workspace: $workspace"
  # Get the last built date
  last_built=$(coder list | grep $workspace | awk '{print $4}')

  echo "last_built: $last_built"

    if [[ ! $last_built == *d*h* ]]
    then
        continue
    fi

  # Get the number of days since the last built date
  days_since_last_built=$(echo $last_built | awk -F 'd' '{print $1}')

  echo "days_since_last_built: $days_since_last_built"

  # If the number of days is greater than 14, delete the workspace
  if [ $days_since_last_built -gt $CLEANUP_DAYS ]
  then
    echo "Deleting workspace $workspace"
    # Add workspace to the list of workspaces to delete
    to_delete+=($workspace)
  fi
done

echo "Delete workspaces: ${to_delete[@]}"

read -p "Do you want to delete the workspaces? (y/n) " -n 1 -r
# Loop through the workspaces to delete
for workspace in ${to_delete[@]}
do
  coder delete -y $workspace
done