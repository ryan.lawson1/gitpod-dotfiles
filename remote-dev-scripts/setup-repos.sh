#!/bin/bash
echo "Setup repos"

shopt -s dotglob

GITLAB_TEXTEMMA_URL='https://gitlab.com/textemma'
# Copy stashed envs
for dir in ~/.config/coderv2/dotfiles/repos/*
do
	echo "Restoring files from $dir"
	PROJECT_DIRECTORY=$(basename $dir)
	PROJECT_PATH="/workspace/remote-dev/$PROJECT_DIRECTORY"
	echo "    Checking Project $PROJECT_DIRECTORY"
	if [ ! -d "$PROJECT_PATH" ]; then
		echo "    $PROJECT_DIRECTORY does not exist, attempting to clone..."
		git clone $GITLAB_TEXTEMMA_URL/$PROJECT_DIRECTORY.git $PROJECT_PATH
	fi
	for f in $dir/*
		do
			FILE_NAME=$(basename $f)
			RESTORE_PATH="$PROJECT_PATH/$FILE_NAME"
			echo "    $f -> $RESTORE_PATH"
			cp -r $f $RESTORE_PATH
		done
done

echo "Exec repo scripts"

# Setup each repo with custom script
for dir in ~/.config/coderv2/dotfiles/repo-scripts/*
do
	PROJECT_DIRECTORY=$(basename $dir)
	PROJECT_PATH="/workspace/remote-dev/$PROJECT_DIRECTORY"
	for f in $dir/*
		do
			FILE_NAME=$(basename $f)
			echo "    Exec $FILE_NAME -> $PROJECT_PATH"
			pushd $PROJECT_PATH
			$f
			popd
		done
done

shopt -u dotglob